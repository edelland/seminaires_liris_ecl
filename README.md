# Seminaires LIRIS@ECL


## Prochain séminaire :

**Date :** <!--A venir--> Vendredi 20 décembre 2024 à 10h

**Lieu (mode hybride) :** 

- Salle : Szulkin, bâtiment E6, 1er étage, Ecully
- Visio : https://ec-lyon-fr.zoom.us/j/97959739701


**Ordre du jour :**  <!--A venir--> 


- Exposé par Di Huang : "Multi-modal Visual Perception" 
- Questions diverses

<!--
- Exposé 1 : "Latent object-centric representation for robotic manipulation" par Alexandre Chapin
- Exposé 2 : "Textured Mesh Quality Assessment: Large-Scale Dataset and Deep Learning-based Quality Metric" par Guillaume Lavoué
-->
<!--_Titre :_--> 


_Résumé :_ 

With the rapid development of the internet and sensing technology, different modalities of data are emerging at an unprecedented speed. Multimodal data convey more comprehensive clues, but due to significant differences in their underlying mechanisms and forms of expression, and the complexity of the coupling relation-ships between modalities, how to utilize and mine the complementary information of multi-modal data has become an important research direction in the field. This talk focuses on visual perception based on multimodal data and introduces the recent research work of the team of the presenter in this direction. The key discussion point is mainly on the fusion of texture and geometric information, involving data collected from various types of devices such as image sensors, depth sensors, LIDAR, for diverse tasks, including, facial analysis, robotic grasping, object detection, with the applications in industry, transportation, and other related areas.

_Biographie :_ 

Di Huang received the B.S. and M.S. degrees in computer science from Beihang University, Beijing, China, in 2005 and 2008, respectively, and the Ph.D. degree in computer science from the École Centrale de Lyon, Lyon, France, in 2011. He joined the Laboratory of Intelligent Recognition and Image Processing, School of Computer Science and Engineering, Beihang University, as a Faculty Member, where he is cur-rently a Professor. His research interests include computer vision, representation learning, robotics, etc. He has published 100+ papers in the major academic journals and conferences such as IEEE-TPAMI, IJCV, CVPR, ICCV, ECCV, with 12,000+ ci-tations and H-index 50. He has been among the top 2% world-wide AI scientist list since released in 2019 (by Stanford University and Elsevier). He was awarded the Best Paper at CCBR 2016 and AMFG 2017, the Best Student Paper at CCBR 2017, the Best Poster Paper at ICB 2016, and the Honorable Mention for Best Student Paper at FG 2024. He was the winner of the Affect Recognition Challenge of AVEC at MM 2013 and the OCRTOC track in Robotic Grasping and Manipulation Competitions at ICRA 2022.He served as a Guest Editor for ACM-TOMM, an Area Chair/Senior PC Member for CVPR 2022/2024-2025, ECCV 2022, MM 2019-2023, IJCAI 2021/2025, WACV 2024-2025, ICPR 2020/2024, IJCB 2021/2023-2024, ICMI 2021, ACII 2019-2023, and a Publicity Chair for FG 2019, IJCB 2020, and FG 2023.

<!-- A venir -->

<!--
Occlusions are very common in face images in the wild, leading to the degraded performance of face-related tasks.  Although much effort has been devoted to removing occlusions from face images, the varying shapes and textures of occlusions still challenge the robustness of current methods. As a result, current methods either rely on manual occlusion masks or only apply to specific occlusions. This paper proposes a novel face de-occlusion model based on face segmentation and 3D face reconstruction, which automatically removes all kinds of face occlusions with even blurred boundaries,e.g., hairs. The proposed model consists of a 3D face reconstruction module, a face segmentation module, and an image generation module. With the face prior and the occlusion mask predicted by the first two, respectively, the image generation module can faithfully recover the missing facial textures. To supervise the training, we further build a large occlusion dataset, with both manually labeled and synthetic occlusions. Qualitative and quantitative results demonstrate the effectiveness and robustness of the proposed method.
-->

## Historique des exposés :

- Di Huang, [Multi-modal Visual Perception](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2024_12_20-Multi_modal_Visual_Perception.pdf), le 20 décembre 2024. 
- Zied Bouyahia, [Towards an integrated framework for smart mobility](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2024_04_08-Towards_an_integrated_framework_for_smart_mobility.pdf), le 8 avril 2024.

- Quentin Gallouedec, [Toward the Generalization of Reinforcement Learning](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2024_03_19-Toward_the_Generalization_of_Reinforcement_Learning.pdf), le 19 mars 2024.

- Guillaume Lavoué, [Textured Mesh Quality Assessment: Large-Scale Dataset and Deep Learning-based Quality Metric](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2023_11_20-Textured_Mesh_Quality_Assessment.pptx), le 20 novembre 2023.

- Alexandre Chapin, [Latent_object_centric_representation_for_robotic_manipulation](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2023_10_16-Latent_object_centric_representation_for_robotic_manipulation.pdf), le 16 octobre 2023.


- Emmanuel Dellandréa, [Présentation rapide du LIRIS](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Pr%C3%A9sentations/2023_09_25-Presentation_LIRIS_ECL.pdf), le 25 septembre 2023.

- Florian Ramousse, [Réalisation d’environnements virtuels pour l’étude et l’amélioration des thérapies des troubles alimentaires](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_06_23-Realisation_d_environnements_virtuels_pour_l_etude_et_l_amelioration_des_therapies_des_troubles_alimentaires.pdf), le 23 juin 2022.

- Mathis Baubriaud, [Automated progress monitoring of construction work using Augmented Reality and Computer Vision](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_06_09-Automated_progress_monitoring_of_construction_work_using_Augmented_Reality_and_Computer_Vision.pdf), le 9 juin 2022.

- Rui Yang, [Data-Efficient Early Knowledge Distillation for Vision Transformers](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_06_01-DearKD__Data-Efficient_Early_Knowledge_Distillation_for_Vision_Transformers.pdf), le 1er juin 2022.

- Théo Jaunet, [Deep Learning Interpretability with Visual Analytics: Exploring Reasoning and Bias Exploitation](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_05_11-Deep_Learning_Interpretability_with_Visual_Analytics.pdf), le 11 mai 2022.

- Nicolas Jacquelin, [Automatic Sports Field Registration](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_03_31-Automatic_Sports_Field_Registration.pdf), le 31 mars 2022.

- Quentin Gallouédec, [The exploration problem in Deep Reinforcement Learning](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_03_17-The_exploration_problem_in_Deep_Reinforcement_Learning.pdf), le 17 mars 2022.

- Jérôme Rutinowski, [Logistics Research – made in Germany](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_03_03-Logistics_Research_made_in_Germany.pdf), le 3 mars 2022.

- Romain Vuillemot, Projet TRACKGUIDE : guidage non-intrusif de l'humain pour la réalisation de mouvements complexes, le 10 février 2022.

- Guillaume Lavoué et Patrick Baert, [Réalités virtuelle et augmentée : usages, enjeux et travaux au sein de l'ECL-ENISE](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_01_27-Realites_virtuelle_et_augmentee.pdf), le 27 janvier 2022.

- Xiangnan Yin, [Segmentation-Reconstruction-Guided Facial Image De-occlusion](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2022_01_13-Segmentation-Reconstruction-Guided_Facial_Image_De-occlusion.pdf), le 13 janvier 2022.

- Emilie Mathian, Halonet : an anomaly detection model to improve the diagnosis of lung carcinoids, le 16 décembre 2021.

- Hugues Moreau, [Deep Learning for Temporal Multidimensional Signals - an Application to Transport Mode Detection](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/blob/master/Présentations/2021_12_02-Deep_Learning_for_Temporal_Multidimensional_Signals.pdf), le 2 décembre 2021.

- Thomas Duboudin, [Discovering "hidden" patterns in data to improve deep networks generalization](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/raw/master/Présentations/2021_11_25-Discovering_hidden_patterns_to_improve_deep_networks_generalization.pdf?inline=false), le 25 novembre 2021.

- Emmanuel Dellandréa, [Présentation rapide du LIRIS](https://gitlab.liris.cnrs.fr/edelland/seminaires_liris_ecl/-/raw/2948f0143f027c6379c1c0d46417095fc1322561/Présentations/2021_11_25-Presentation_LIRIS_ECL.pdf), le 25 novembre 2021.




